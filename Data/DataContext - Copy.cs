﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace TestApsProject.Data
//{
//    public class DataContext
//    {
//        private static Dictionary<string, Models.Person> dictionary;
//        private string dataPath = @"Data/sample.json";

//        public DataContext()
//        {
//            if (dictionary == null)
//            {
//                string json = System.IO.File.ReadAllText(dataPath);
//                dictionary = System.Text.Json.JsonSerializer.Deserialize<List<Models.Person>>(json)
//                                                        .ToDictionary(person => person._id);
//            }
//        }

//        private void UpdateJson()
//        {
//            string json = System.Text.Json.JsonSerializer
//                .Serialize<List<Models.Person>>(dictionary.Values.ToList<Models.Person>());
//            System.IO.File.WriteAllText(dataPath, json);
//        }

//        public List<Models.Person> GetPeople()
//        {
//            return dictionary.Values.ToList();
//        }

//        public Models.Person GetPerson(string id)
//        {
//            return dictionary[id];
//        }

//        public bool DeletePerson(string id)
//        {
//            if (dictionary.Remove(id))
//            {
//                UpdateJson();
//                return true;
//            }

//            return false;
//        }

//        public bool Replace(Models.Person person)
//        {
//            if (dictionary.TryGetValue(person._id, out Models.Person prev_person))
//            {
//                dictionary[person._id] = person;
//                UpdateJson();

//                return true;
//            }

//            return false;
//        }

//        public bool Add(Models.Person person)
//        {
//            try
//            {
//                dictionary.Add(person._id, person);
//            }
//            catch (ArgumentException)
//            {
//                return false;
//            }
//            UpdateJson();

//            return true;
//        }
//    }
//}
