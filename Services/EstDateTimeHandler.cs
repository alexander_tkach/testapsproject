﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApsProject.Services
{
    public class EstDateTimeHandler : IDateTimeHandler
    {
        public string ConvertDateTimeToString(DateTime dateTime)
        {
            DateTime timeUtc = dateTime.ToUniversalTime();
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);
            return easternTime.ToString("t") + " по Est";
        }
    }
}
