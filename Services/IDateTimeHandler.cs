﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApsProject.Services
{
    public interface IDateTimeHandler
    {
        string ConvertDateTimeToString(DateTime dateTime);
    }
}
