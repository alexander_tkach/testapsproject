﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApsProject.Services
{
    public class CurrentDateTimeHandler : IDateTimeHandler
    {
        public string ConvertDateTimeToString(DateTime dateTime)
        {
            return dateTime.ToString("t") + " по Киеву";
        }
    }
}
