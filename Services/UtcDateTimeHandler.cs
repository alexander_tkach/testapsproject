﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApsProject.Services
{
    public class UtcDateTimeHandler : IDateTimeHandler
    {
        public string ConvertDateTimeToString(DateTime dateTime)
        {
            return dateTime.ToUniversalTime().ToString("t") + " по Utc";
        }
    }
}
