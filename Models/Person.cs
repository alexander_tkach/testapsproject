﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace TestApsProject.Models
{
    public class Name
    {
        [Key]
        public int NameId { get; set; }
        public string first { get; set; }
        public string last { get; set; }

        //public int PersonId { get; set; }
        //public Person Person { get; set; }

        public override string ToString()
        {
            return $"{first} {last}";
        }
    }

    public class Friend
    {
        [Key]
        public int FriendId { get; set; }
        public int id { get; set; }
        public string name { get; set; }

        //public int PersonId { get; set; }
        //public Person Person { get; set; }

        public override string ToString()
        {
            return $"id: {id}, name: {name}";
        }
    }

    public class Person
    {
        [Key]
        //public int PersonId { get; set; }
        public string _id { get; set; }
        public int index { get; set; }
        public string balance { get; set; }
        public string picture { get; set; }
        public int age { get; set; }
        public string eyeColor { get; set; }
        public Name name { get; set; }
        public string company { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string about { get; set; }
        public List<string> tags { get; set; }
        public List<Friend> friends { get; set; }
        public string favoriteFruit { get; set; }
    }
}
