﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;

//namespace TestApsProject.Controllers
//{
//    public class PersonController : Controller
//    {
//        public IActionResult Index()
//        {
//            return View(new Data.DataContext().GetPeople());
//        }


//        [HttpGet]
//        public ActionResult Details(string id)
//        {
//            return View(new Data.DataContext().GetPerson(id));
//        }


//        [HttpGet]
//        public ActionResult Delete(string id)
//        {
//            return View(new Data.DataContext().GetPerson(id));
//        }

//        [HttpPost]
//        public ActionResult DeletePerson(string id)
//        {
//            new Data.DataContext().DeletePerson(id);

//            return RedirectToAction("Index");
//        }


//        [HttpGet]
//        public ActionResult Edit(string id)
//        {
//            return View(new Data.DataContext().GetPerson(id));
//        }

//        [HttpPost]
//        public ActionResult Edit(Models.Person person)
//        {
//            new Data.DataContext().Replace(person);

//            return RedirectToAction("Index");
//        }


//        [HttpGet]
//        public ActionResult Add()
//        {
//            return View();
//        }

//        [HttpPost]
//        public ActionResult Add(Models.Person person)
//        {
//            person.tags = new List<string>();
//            person.friends = new List<Models.Friend>();
//            new Data.DataContext().Add(person);

//            return RedirectToAction("Index");
//        }
//    }
//}