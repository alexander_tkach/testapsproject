﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Microsoft.EntityFrameworkCore;

namespace TestApsProject.Controllers
{
    public class PersonController : Controller
    {
        public IActionResult Index()
        {
            List<Models.Person> people;
            using (var dataContext = new Data.DataContext())
            {
                people = dataContext.People.Include(p => p.name).ToList();
            }
            return View(people);
        }


        [HttpGet]
        public ActionResult Details(string id)
        {
            Models.Person person;
            using (var dataContext = new Data.DataContext())
            {
                person = dataContext.People
                    .Include(p => p.name)
                    .Include(p => p.friends)
                    .FirstOrDefault(p => p._id == id);
            }
            return View(person);
        }


        [HttpGet]
        public ActionResult Delete(string id)
        {
            Models.Person person;
            using (var dataContext = new Data.DataContext())
            {
                person = dataContext.People
                    .Include(p => p.name)
                    .Include(p => p.friends)
                    .FirstOrDefault(p => p._id == id);
            }
            return View(person);
        }

        [HttpPost]
        public ActionResult DeletePerson(string id)
        {
            using (var dataContext = new Data.DataContext())
            {
                Models.Person person = dataContext.People
                    .Include(p => p.name)
                    .Include(p => p.friends)
                    .FirstOrDefault(p => p._id == id);
                dataContext.People.Remove(person);
                dataContext.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Edit(string id)
        {
            Models.Person person;
            using (var dataContext = new Data.DataContext())
            {
                person = dataContext.People
                    .Include(p => p.name)
                    .Include(p => p.friends)
                    .FirstOrDefault(p => p._id == id);
            }
            return View(person);
        }

        [HttpPost]
        public ActionResult Edit(Models.Person person)
        {
            person.tags.Remove(null);
            Models.Person person_to_edit;
            using (var dataContext = new Data.DataContext())
            {
                person_to_edit = dataContext.People.Find(person._id);
                dataContext.Entry(person_to_edit).State = EntityState.Detached;
                dataContext.People.Attach(person);
                dataContext.Entry(person).State = EntityState.Modified;

                dataContext.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Models.Person person)
        {
            person.tags = new List<string>();
            person.friends = new List<Models.Friend>();
            using (var dataContext = new Data.DataContext())
            {
                dataContext.Add(person);
                dataContext.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}