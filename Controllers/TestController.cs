﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using TestApsProject.Services;

namespace TestApsProject.Controllers
{
    public class TestController : Controller
    {
        private IDateTimeHandler dateTimeHandler;
        public TestController(IDateTimeHandler handler)
        {
            dateTimeHandler = handler;
        }

        public IActionResult Index()
        {
            return View();
        }

        public string GetString()
        {
            return "Hello";
        }

        public string Hello(string name)
        {
            return $"Hello, {name}.";
        }

        public string GetDayOfYear()
        {
            return DateTime.Now.DayOfYear.ToString();
        }

        public string GetTime()
        {
            //int hour = DateTime.Now.Hour;
            //string message;

            //if (hour < 5)
            //{
            //    message = "Доброй ночи!";
            //}
            //else if (hour < 12)
            //{
            //    message = "Доброе утро!";
            //}
            //else if (hour < 16)
            //{
            //    message = "Добрый день!";
            //}
            //else
            //{
            //    message = "Добрый вечер!";
            //}

            //return $"{message} Сейчас {DateTime.Now.ToString("t")}.";

            return dateTimeHandler.ConvertDateTimeToString(DateTime.Now);
        }

        public string Sum(int a, int b)
        {
            return (a + b).ToString();
        }

        public string ErrorMaker()
        {
            int a = 0;
            int b = 1 / a;

            return "))))";
        }
    }
}