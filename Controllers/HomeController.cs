﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System.Text.Json;

namespace TestApsProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("~/Person");
        }

        public IActionResult Hello(string name)
        {
            ViewBag.Name = name;
            return View();
        }

        public IActionResult GetData()
        {
            string json = System.IO.File.ReadAllText(@"Data\sample.json");
            List<Models.Person> people = JsonSerializer.Deserialize<List<Models.Person>>(json);

            return View(people);
        }
    }
}
