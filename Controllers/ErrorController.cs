﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TestApsProject.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index(int code)
        {
            ViewBag.ErrorCode = code;
            
            return View();
        }
    }
}